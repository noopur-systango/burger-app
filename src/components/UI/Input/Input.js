import React from 'react';
import './Input.css';

const Input = (props) => 
{
    return(
        <div>
            <input 
            className={'Input'} 
            inputtype="input" 
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.changed}
            name={props.name}
            /><br></br>
             <span><br></br></span>   
        </div>
    );
}

export default Input;