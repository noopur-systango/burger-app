import React from 'react';
import burgerlogo from '../../assests/images/logo.png';
import './Logo.css';

const logo = (props) => 
(
    <div className={'Logo'} style={{height:props.height}}>
        <img src={burgerlogo} alt="logo"/>
    </div>
);

export default logo;