import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import axios from "../../axios-config";
import Spinner from "../../components/UI/Spinner/Spinner";

const masala_prices = {
  salad: 10,
  cheese: 20,
  aaloo: 30,
  bacon: 40
};

class BurgerBuilder extends Component {
  state = {
    masala: null,
    totalPrice: 10,
    purchasable: false,
    purchasing: false,
    loading: false
  };

  componentDidMount() {
    //console.log(this.props);
    axios
      .get("/masala.json")
      .then(response => {
        this.setState({ masala: response.data });
      })
      .catch();
  }

  updatePurchaseState(masala) {
    const sum = Object.keys(masala)
      .map(mKey => {
        return masala[mKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);
    this.setState({ purchasable: sum > 0 });
  }

  addMasalaHandler = type => {
    const oldCount = this.state.masala[type];
    const updateCount = oldCount + 1;
    const updatedMasala = {
      ...this.state.masala
    };

    updatedMasala[type] = updateCount;
    const priceaddition = masala_prices[type];
    const oldprice = this.state.totalPrice;
    const newprice = oldprice + priceaddition;
    this.setState({ totalPrice: newprice, masala: updatedMasala });
    this.updatePurchaseState(updatedMasala);
  };

  removeMasalaHandler = type => {
    const oldCount = this.state.masala[type];
    if (oldCount <= 0) {
      return;
    }

    const updateCount = oldCount - 1;
    const updatedMasala = {
      ...this.state.masala
    };

    updatedMasala[type] = updateCount;
    const priceaddition = masala_prices[type];
    const oldprice = this.state.totalPrice;
    const newprice = oldprice - priceaddition;
    this.setState({ totalPrice: newprice, masala: updatedMasala });
    this.updatePurchaseState(updatedMasala);
  };

  purchaseHandler = () => {
    this.setState({ purchasing: true });
  };

  purchaseHandlerClose = () => {
    this.setState({ purchasing: false });
  };

  purchaseHandlerContinue = () => {
    //alert("Order Placed...!!")
      console.log(this.props);

      const queryParamas =[];
      for(let i in this.state.masala)
      {
      queryParamas.push(encodeURIComponent(i) + "=" + encodeURIComponent(this.state.masala[i]))
      }
      queryParamas.push("price=" + this.state.totalPrice)
      const queryString = queryParamas.join('&')
      this.props.history.push({pathname: '/checkout', search: '?'+ queryString});
  };

  render() {
    const disabledInfo = {
      ...this.state.masala
    };
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }
    let ordersummary = null;

    const { masala, totalPrice, purchasable ,purchasing } = this.state;

    let burger = <Spinner />;

    if (this.state.masala) {
      burger = (
        <Aux>
          <Burger masala={ masala }></Burger>
          <BuildControls
            masalaadded={this.addMasalaHandler}
            masalaremoved={this.removeMasalaHandler}
            disabled={disabledInfo}
            purchasable={ purchasable }
            price={ totalPrice } 
            ordered={this.purchaseHandler}
          />
        </Aux>
      );
      ordersummary = (
        <OrderSummary
          masala={masala}
          price={totalPrice}
          purchasecancel={this.purchaseHandlerClose}
          purchasecontinue={this.purchaseHandlerContinue}
        />
      );
    }

    if (this.state.loading) {
      ordersummary = <Spinner />;
    }
    return (
      <Aux>
        <Modal
          show={ purchasing }
          modalClosed={this.purchaseHandlerClose}
        >
          {ordersummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

export default BurgerBuilder;
