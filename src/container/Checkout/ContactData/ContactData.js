import React, { Component } from "react";
import Button from "../../../components/UI/Button/Button";
import "./ContactData.css";
import axios from "../../../axios-config";
import Input from "../../../components/UI/Input/Input";

class ContactData extends Component {
  state = {
    orderForm: {
      name: {
        value: "",
        valid: false,
        validation:
        {
          required: true
        }
      },
      number: {
        value: "",
        valid: false,
        validation:
        {
          required: true,
          minLength: 10,
          maxLength: 10
        }
      },
      street: {
        value: "",
        valid: false,
        validation:
        {
          required: true
        }
      },
      email: {
        value: "",
        valid: false,
        validation:
        {
          required: true
        }
      }
    }
  };
  orderHandler = event => {
    event.preventDefault();
    const { orderForm } = this.state;
    const { masala, price } = this.props; //destructing
    const order = {
      masala: { masala },
      price: { price },
      orderData: { orderForm } //sending form data to server
    };

    axios
      .post("/order.json", order)
      .then(response => {
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
      });
  };

  checkValidity (value,rules) {
    let isValid = true;

    if(rules.required)
    {
      isValid = value.trim() !== '' && isValid;
    }

    if(rules.minLength)
    {
      isValid = value.length >= rules.minLength && isValid;
    }

    if(rules.maxLength)
    {
      isValid = value.length <= rules.maxLength && isValid;
    }
    return isValid;

  }

  inputChangedHandler = (event, key) => {
    const updatedOrderForm = {
      ...this.state.orderForm     //extracts name,number....
    };

    const updatedFormElement = {
      ...updatedOrderForm[key]   //extracts "value" of each key
    };

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation)
    updatedOrderForm[key] = updatedFormElement;
    //console.log(updatedFormElement);
    this.setState({ orderForm: updatedOrderForm });
  };
  render() {
    let form = (
      <form>
        {Object.keys(this.state.orderForm).map(key => (
          <Input
            key={key}
            name={key}
            placeholder={" Your " + key}
            changed={event => {
              this.inputChangedHandler(event, key);
            }}
          ></Input>
          
        ))}
        <Button btnType="Success" clicked={this.orderHandler}>
          Order
        </Button>
      </form>
    );

    return (
      <div className={"ContactData"}>
        <h4>Enter your contact data:</h4>
        {form}
      </div>
    );
  }
}

export default ContactData;
