import React,{Component} from 'react';
import Checkoutsummary from '../../components/OrderSummary/CheckoutSummary';
import {Route} from 'react-router-dom'; 
import ContactData from '../../container/Checkout/ContactData/ContactData';

class Checkout extends Component{
    state = {
        masala: null,
        price: 0
    };

    componentWillMount = () => {
       const query = new URLSearchParams(this.props.location.search);
        const masala = {};
        let price = 0;
        for(let param of query.entries())
        {
            if(param[0] === 'price')
            {
                price = param[1]
            }
            else{
                masala[param[0]] = +param[1];
            }     
        }
        
        this.setState({masala: masala,totalPrice: price});  
    }

    checkoutCancelHandler = () => 
    {
        this.props.history.goBack();
    }

    checkoutContinueHandler = () => 
    {
        this.props.history.replace('/checkout/contact-data');
    }


    render(){
        return(
            <div>
                <Checkoutsummary masala={this.state.masala}
                checkoutCancel={this.checkoutCancelHandler}
                checkoutContinue={this.checkoutContinueHandler}/>
                 <Route path={this.props.match.path + '/contact-data'} 
                 render={() => (<ContactData masala={this.state.masala} price={this.state.totalPrice} />)}></Route>
            </div>
        );
    }
} 

export default Checkout;

